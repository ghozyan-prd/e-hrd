<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AssettransactionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('asset_transactions')->insert([
            [
              'kode_peminjaman' => 'PA01',
              'id_aset' => '1',
              'id_pegawai' => '1',
              'tanggal_pinjam' => '2019-12-26 ',
              'tanggal_kembali' => '2019-12-26 ',
              'created_at' => '2019-12-26 ',
              'updated_at' => '2019-12-26 ',
              ],
              [
              'kode_aset' => 'PA02',
              'id_aset' => '1',
              'id_pegawai' => '2',
              'tanggal_pinjam' => '2019-12-26',
              'tanggal_kembali' => null,
              'created_at' => '2019-12-26 ',
              'updated_at' => '2019-12-26 ',
                ],

            ]);
    }
}
