<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         //employee

         $this->call(corporateTableSeeder::class);
         $this->call(GradesTableSeeder::class);
         $this->call(PostulantsTableSeeder::class);
         $this->call(DivisionTableSeeder::class);
         $this->call(PositionsTableSeeder::class);
         $this->call(EmployeesTableSeeder::class);

        //interviewer
        $this->call(ReferencesTableSeeder::class);
        $this->call(OrganizationTableSeeder::class);
        $this->call(LanguagesTableSeeder::class);
        $this->call(TrainingsTableSeeder::class);
        $this->call(JobhistorysTableSeeder::class);
        $this->call(AwardsTableSeeder::class);
        $this->call(FamilysTableSeeder::class);
        $this->call(EmergencycontactTableSeeder::class);
        $this->call(EducationsTableSeeder::class);
        $this->call(AppsCountriesSeeder::class);
    //Aset
        $this->call(AssetsTableSeeder::class);
        $this->call(AssettransactionsTableSeeder::class);

    //Cuti

        $this->call(CuticategoryTableSeeder::class);
        $this->call(CutirequestTableSeeder::class);
        
        $this->call(SalariesTableSeeder::class);

}
}
