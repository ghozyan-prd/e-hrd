<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CuticategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cuti_categorys')->insert([
            [
                'kode_kategori_cuti' => 'CC01',
                'jenis_cuti' => 'sakit',
                'keterangan' => 'gapapa',
                'status' => 'True',
                'created_at' => '2019-12-26 00:00:00',
                'updated_at' => '2019-12-26 00:00:00',
                ],
                [
                'kode_kategori_cuti' => 'CC02',
                'jenis_cuti' => 'izin',
                'keterangan' => 'lalala',
                'status' => 'False',
                'created_at' => '2019-12-26 00:00:00',
                'updated_at' => '2019-12-26 00:00:00',
                ],

            ]);
    }
}
