<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salaries', function (Blueprint $table) {
            $table->bigIncrements('id')->unique();
            $table->char('Tahun', 100)->nullable();
            $table->char('Bulan', 100)->nullable();
            $table->char('ID_Pegawai', 100)->nullable();
            $table->char('Nama_Pegawai', 100)->nullable();
            $table->char('Kehadiran', 2)->nullable();
            $table->char('Jumlah_Cuti_Tak_Dibayar', 2)->nullable();
            $table->char('Gaji_Pensiun', 10)->nullable();
            $table->char('Tunjangan_Harian', 10)->nullable();
            $table->char('Tunjangan_Lainnya_Uang_Lembur_dsb', 10)->nullable();
            $table->char('Cuti_Tidak_Dibayar', 10)->nullable();
            $table->char('Bonus_Tak_Tetap', 10)->nullable();
            $table->char('Jumlah_Penghasilan_Bruto', 10)->nullable();
            $table->char('Jumlah_Pengurangan', 10)->nullable();
            $table->char('PPh_21_sebulan', 10)->nullable();
            $table->char('Uang_Pembayaran_Gaji', 10)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('salaries');
    }
}
