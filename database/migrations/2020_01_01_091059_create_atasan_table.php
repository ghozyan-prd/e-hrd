<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAtasanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('superiors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('kode_atasan',50)->unique();
            $table->char('nama_atasan',50);
            $table->unsignedBigInteger('id_jabatan')->nullable();
            $table->timestamps();

            $table->foreign('id_jabatan')
            ->references('id')
            ->on('positions')
            ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('atasan');
    }
}
