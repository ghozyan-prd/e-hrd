<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePostulantsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('postulants', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->char('nama_lengkap', 100)-> nullable();
            $table->char('posisi_yang_dilamar', 100)-> nullable();
            $table->char('tempat_lahir', 100)-> nullable();
            $table->date('tanggal_lahir')-> nullable();
            $table->char('no_ktp', 100)-> nullable();
            $table->char('jenis_kelamin', 100)-> nullable();
            $table->char('status_perkawinan', 100)-> nullable();
            $table->char('agama', 100)-> nullable();
            $table->char('kebangsaan', 100)-> nullable();
            $table->dateTime('tanggal_join')-> nullable();
            $table->char('npwp', 100)-> nullable();
            $table->char('alamat_domisili', 100)-> nullable();
            $table->char('kota_domisili', 100)-> nullable();
            $table->char('alamat_ktp', 100)-> nullable();
            $table->char('kota_ktp', 100)-> nullable();
            $table->char('telepon', 100)-> nullable();
            $table->char('email', 100);
            $table->string('password', 255);
            $table->char('kendaraan', 100)-> nullable();
            $table->char('laptop', 100)-> nullable();
            $table->integer('gaji_diharap')-> nullable();
            $table->integer('anak_ke')-> nullable();
            $table->integer('saudara')-> nullable();
            $table->char('pencapaian_sudah_dilakukan', 255)-> nullable();
            $table->char('pencapaian_yang_diinginkan', 255)-> nullable();
            $table->mediumText('foto')-> nullable();
            $table->text('catatan')-> nullable();
            $table->char('status_lamaran', 255)-> nullable();
            $table->char('alasan', 255)-> nullable();
            $table->char('form_lamaran_lengkap', 3)-> nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('postulants');
    }
}
