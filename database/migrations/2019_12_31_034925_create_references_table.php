<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReferencesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('references', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('id_postulant')->nullable();
            $table->char('nama', 100);
            $table->char('perusahaan', 100)->nullable();
            $table->char('jabatan', 100)->nullable();
            $table->char('telepon', 100)->nullable();
            $table->timestamps();

            $table->foreign('id_postulant')
                  ->references('id')
                  ->on('postulants')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('references');
    }
}
