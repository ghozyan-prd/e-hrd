@extends('layouts.master')

@section('content')

<div class="page-title">
              <div class="title_left">
                <h3>KARYAWAN <small>PT Davinti Indonesia</small></h3>
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5  form-group pull-right top_search">
                  <div class="input-group">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                      <button class="btn btn-default" type="button">Go!</button>
                    </span>
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="x_panel">
                  <div class="x_content">
                      <div class="col-md-12 col-sm-12  text-center">
                        <ul class="pagination pagination-split">
                          <li>
                            <a href="/admin/employees/create" class="btn btn-custon-four btn-success text-white"><i class="fa fa-reply"></i> Tambah Karyawan</a>
                          </li>
                        </ul>
                      </div>

                      <div class="clearfix"></div>
                      @foreach ($employees as $v => $ast )
                      <div class="col-md-4 col-sm-4  profile_details">
                        <div class="well profile_view">
                          <div class="col-sm-12">
                            <h4 class="brief"><i>{{@$ast -> position->nama_jabatan }}</i></h4>
                            <div class="left col-md-7 col-sm-7">
                              <h2>{{$ast -> nama}}</h2>
                              <p><strong>About: </strong> Web Designer / UX / Graphic Artist / Coffee Lover </p>
                              <ul class="list-unstyled">
                                <li><i class="fa fa-building"></i> Address: {{@$ast ->postulant-> alamat_domisili}}</li>
                                <li><i class="fa fa-phone"></i> Phone #: {{@$ast -> postulant->telepon}}</li>
                              </ul>
                            </div>
                            <div class="right col-md-5 col-sm-5 text-center">
                              <img src="{{url('images/personal/' . $ast ->postulant->foto)}}"  alt="" class="img-circle img-fluid" width="100px;" height="100px;">
                            </div>
                          </div>
                          <div class=" profile-bottom text-center">
                            <div class=" col-sm-6 emphasis">

                            </div>
                            <div class=" col-sm-6 emphasis">
                              <button type="button" class="btn btn-primary btn-sm" onclick="window.location.href='/admin/employees/{{$ast->id}}'">
                                <i class="fa fa-user"> </i> View Profile
                              </button>

                            </div>
                          </div>
                        </div>
                      </div>
                      @endforeach
                  </div>
                </div>
            </div>

@endsection
