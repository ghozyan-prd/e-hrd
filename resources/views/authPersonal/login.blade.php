<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Log In PT Davinti Indonesia</title>

    <!-- Font Icon -->
    <link rel="stylesheet" href="../materiallogin/fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="../materiallogin/css/style.css">
</head>
<body>


        <!-- Sing in  Form -->
        <section>
            <div class="container">
                <div class="signin-content">
                  @if(session()->has('Alert'))
                      <script>
                          alert({{ session()->get('Alert') }});
                      </script>
                  @endif
                    <div class="signin-image">
                        <figure><img src="../materiallogin/images/signin-image.jpg" alt="sing up image"></figure>
                        <a href="{{ route('personal.register') }}" class="signup-image-link">{{ __('Create an Account') }}</a>
                    </div>

                    <div class="signin-form">
                        <h2 class="form-title">Sign Personal</h2>
                        <form action="{{ route('personal.login.submit') }}" method="POST" class="register-form" id="login-form">
                          @csrf
                            <div class="form-group">
                                <label for="your_name"><i class="zmdi zmdi-account material-icons-name"></i></label>
                                <input type="email" name="email"  id="email" value="{{ old('email') }}" required autocomplete="email" autofocus/>

                            </div>
                            <div class="form-group">
                                <label for="your_pass"><i class="zmdi zmdi-lock"></i></label>
                                <input type="password" name="password"  id="your_pass" name="password" required autocomplete="current-password"/>

                            </div>
                            <div class="form-group form-button">
                                <input type="submit" name="signin" id="signin" class="form-submit" value="Log in"/>

                            </div>
                        </form>
                        <div class="social-login">
                            <a href="{{ route('personal.password.request') }}" class="signup-image-link">{{ __('Forgot Your Password?') }}</a>

                        </div>
                    </div>
                </div>
            </div>
        </section>
    <!-- JS -->

    <script src="../materiallogin/vendor/jquery/jquery.min.js"></script>
    <script src="../materiallogin/js/main.js"></script>
    @include('sweetalert::alert')
</body><!-- This templates was made by Colorlib (https://colorlib.com) -->
</html>
