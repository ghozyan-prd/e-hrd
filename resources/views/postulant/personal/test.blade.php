
<!-- Multi step form -->

@extends('layouts.postulant')

@section('content')
@if($cek <= 0 && $cekall >= 10)
<div class="clearfix"></div>

  <div class="row">
              <div class="col-md-12 col-sm-12 ">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Soal Test Kepribadian</h2>
                    <!-- <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                        <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="#">Settings 1</a>
                            <a class="dropdown-item" href="#">Settings 2</a>
                          </div>
                      </li>
                      <li><a class="close-link"><i class="fa fa-close"></i></a>
                      </li>
                    </ul> -->
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    @if(session()->has('Alert'))
                        <script>
                            alert({{ session()->get('Alert') }});
                        </script>
                    @endif

                    <div class="col-md-12 col-sm-12 ">
                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <!-- <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Halaman 1</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Halaman 2</a>
                          </li>
                          <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">Halaman 3</a>
                          </li>
                        </ul> -->
                        <form class="form-horizontal form-label-left" method="post" action="/personal/personality_test">
                          @csrf
                        <div id="myTabContent" class="tab-content">

                          <div role="tabpanel" class="tab-pane active " id="tab_home" aria-labelledby="soal1-home">
                            <!-- start recent activity -->
                          <div class="row">
                            <div class="col-md-12 col-sm-12">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Peraturan Mengerjakan Test Kepribadian</h2>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <br />

                                  <div class="form-group row">
                                    <label class="col-md-1 col-sm-1  control-label label-align">1.
                                    </label>
                                    <div class="col-md-11 col-sm-11 ">
                                      <h4>Ada sembilan puluh ( 90 ) pasang,pilihlah satu dari setiap pasangan tersebut yang Saudara anggap paling dekat menggambarkan diri saudara.</h4>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-md-1 col-sm-1  control-label label-align">2.
                                    </label>
                                    <div class="col-md-11 col-sm-11 ">
                                      <h4>Bila tidak satupun dari sebuah pasangan pernyataan yang cocok, pilihlah yang Saudara anggap paling mendekati</h4>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-md-1 col-sm-1  control-label label-align">3.
                                    </label>
                                    <div class="col-md-11 col-sm-11 ">
                                      <h4>Isilah jawaban pada kolom yang sudah disediakan  pada setiap pernyataan yang Saudara pilih.</h4>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-md-1 col-sm-1  control-label label-align">4.
                                    </label>
                                    <div class="col-md-11 col-sm-11 ">
                                      <h4>Sebagai Contoh, dalam hal ini Saudara memilih pernyataan " b ", karena pernyataan " b " merupakan/mendekati gambaran diri Saudara.</h4>
                                      <div class="radio">
                                        <label>
                                          <input type="radio" class="flat" name="iCheck">Saya adalah pekerja keras
                                        </label>
                                      </div>
                                      <div class="radio">
                                        <label>
                                          <input type="radio" class="flat" checked name="iCheck"> Saya tidak mudah murung
                                        </label>
                                      </div>
                                      <br>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-md-1 col-sm-1  control-label label-align">5.
                                    </label>
                                    <div class="col-md-11 col-sm-11 ">
                                      <h4>Kerjakan secara urut, secepat mungkin dan pilihlah <b>hanya satu</b> pernyataan dari tiap pasang.</h4>
                                    </div>
                                  </div>

                                  <div class="form-group row">
                                    <label class="col-md-1 col-sm-1  control-label label-align">6.
                                    </label>
                                    <div class="col-md-11 col-sm-11 ">
                                      <h4>Pastikan saudara tersambung dengan internet</h4>
                                    </div>
                                  </div>

                                  <div class="ln_solid"></div>
                                    <div class="form-group">
                                      <div class="col-md-9 col-sm-9  offset-md-3">
                                        <a class="btn btn-danger pull-right" href="#tab_soal1" id="soal1-tab" role="tab" data-toggle="tab" aria-expanded="true">MULAI</a>
                                      </div>
                                    </div>

                                </div>
                              </div>
                            </div>
                          </div>
                            <!-- end recent activity -->
                          </div>

                          <div role="tabpanel" class="tab-pane fade " id="tab_soal1" aria-labelledby="soal1-tab">
                            <!-- start recent activity -->
                          <div class="row">
                            <div class="col-md-12 col-sm-12">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Bagian 1</h2>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <br />
                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">1*
                                      </label>

                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ1" value="1" class="flat"  > Saya adalah pekerja keras
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ1" value="0" class="flat"  > Saya tidak mudah murung
                                          </label>
                                        </div>
                                      </div>
                                    </div>


                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">2*
                                      </label>

                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ2" value="1" class="flat"  > Saya ingin bekerja lebih baik daripada orang lain
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ2" value="0" class="flat"  > Saya ingin melaksanakan apa yang saya kerjakan sampai selesai
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">3*
                                      </label>

                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ3" value="1" class="flat"  > Saya suka menunjukkan kepada orang lain bagaimana cara mengerjakan sesuatu
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ3" value="0" class="flat"  > Saya ingin berusaha sebaik mungkin
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">4*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ4" value="1" class="flat"  > Saya melakukan hal-hal yang lucu
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ4" value="0" class="flat"  >  Saya suka mengatakan kepada orang lain apa yang harus dikerjakan
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">5*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ5" value="1" class="flat"  > Saya suka bergabung didalam kelompok
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ5" value="0" class="flat"  > Saya ingin diperhatikan didalam kelompok
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">6*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ6" value="1" class="flat"  > Saya suka membuat persahabatan yang akrab
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ6" value="0" class="flat"  > Saya suka berteman dalam suatu kelompok
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">7*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ7" value="1" class="flat"  > Saya suka membuat persahabatan yang akrab
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ7" value="0" class="flat"  > Saya suka berteman dalam suatu kelompok
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">8*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ8" value="1" class="flat"  > Saya suka membalas jika benar-benar disakiti
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ8" value="0" class="flat"  > Saya suka mengerjakan sesuatu hal yang baru dan berbeda
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">9*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ9" value="1" class="flat"  > Saya ingin disukai atasan saya
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ9" value="0" class="flat"  > Saya suka memberitahu orang lain jika mereka berbuat salah
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">10*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ10" value="1" class="flat"> Saya senang mengikuti petunjuk-petunjuk yang diberikan kepada saya
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ10" value="0" class="flat"> Saya ingin menyenangkan atasan-atasan saya
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">11*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ11" value="1" class="flat"> Saya berusaha keras
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ11" value="0" class="flat"> Saya adalah seorang yang rapi saya menempatkan segala sesuatu ditempatnya
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">12*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ12" value="1" class="flat"> Saya mampu mempengaruhi orang lain untuk melakukan apa yang saya kehendaki
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ12" value="0" class="flat"> Saya tidak mudah marah
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">13*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ13" value="1" class="flat"> Saya suka memberitahukan apa yang harus dikerjakan oleh kelompok
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ13" value="0" class="flat"> Saya selalu mengerjakan tugas sampai selesai
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">14*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ14" value="1" class="flat"> Saya ingin kelihatan gembira dan menarik
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ14" value="0" class="flat"> Saya ingin betul-betul berhasil
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">15*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ15" value="1" class="flat"> Saya suka menyesuaikan diri dalam kelompok
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ15" value="0" class="flat"> Saya suka membantu orang dalam memutuskan sesuatu
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                      <div class="form-group">
                                        <div class="col-md-9 col-sm-9  offset-md-3">
                                          <!-- <a class="btn btn-success pull-right" href="#tab_home" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Kembali</a> -->
                                          <a class="btn btn-success pull-right" href="#tab_soal2" id="soal2-tab" role="tab" data-toggle="tab" aria-expanded="true">Selanjutnya</a>
                                        </div>
                                      </div>

                                </div>
                              </div>
                            </div>
                          </div>
                            <!-- end recent activity -->
                          </div>

                          <div role="tabpanel" class="tab-pane fade " id="tab_soal2" aria-labelledby="soal2-tab">
                            <!-- start recent activity -->
                          <div class="row">
                            <div class="col-md-12 col-sm-12">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Bagian 2</h2>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <br />
                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">16*
                                      </label>

                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ16" value="1" class="flat"> Saya cemas bila ada yang tidak menyukai saya
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ16" value="0" class="flat"> Saya senang bila diperhatikan orang
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">17*
                                      </label>

                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ17" value="1" class="flat"> Saya senang mencoba hal-hal yang baru
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ17" value="0" class="flat"> Saya lebih suka bekerja dengan orang lain daripada sendirian
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">18*
                                      </label>

                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ18" value="1" class="flat"> Saya kadang-kadang menyalahkan orang lain bila ada sesuatu yang tidak beres
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ18" value="0" class="flat"> Saya merasa terganggu bila ada yang tidak menyukai saya
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">19*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ19" value="1" class="flat"> Saya ingin menyenangkan atasan-atasan saya
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ19" value="0" class="flat"> Saya suka mencoba suatu pekerjaan yang baru dan berbeda
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">20*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ20" value="1" class="flat"> Saya suka penjelasan-penjelasan terperinci dalam bekerja
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ20" value="0" class="flat"> Saya suka berterus terang bila ada orang yang menjengkelkan saya
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">21*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ21" value="1" class="flat"> Saya selalu berusaha keras
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ21" value="0" class="flat"> Saya suka mengerjakan setiap langkah pekerjaan dengan hati-hati
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">22*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ22" value="1" class="flat"> Saya memimpin dengan baik
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ22" value="0" class="flat"> Saya mengatur pekerjaan dengan baik
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">23*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ23" value="1" class="flat"> Saya mudah marah
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ23" value="0" class="flat"> Saya lamban dalam membuat keputusan-keputusan
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">24*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ24" value="1" class="flat"> Saya senang mengerjakan beberapa tugas pada saat yang sama
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ24" value="0" class="flat"> Dalam kelompok, saya lebih suka diam
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">25*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ25" value="1" class="flat"> Saya senang diundang
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ25" value="0" class="flat"> Saya ingin mengerjakan segala sesuatunya lebih baik dari orang lain
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">26*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ26" value="1" class="flat"> Saya suka membuat persahabatan yang akrab
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ26" value="0" class="flat"> Saya suka memberi nasehat kepada orang lain
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">27*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ27" value="1" class="flat"> Saya suka mengerjakan sesuatu hal yang baru dan berbeda
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ27" value="0" class="flat"> Saya senang menceritakan keberhasilan saya
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">28*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ28" value="1" class="flat"> Jika memang saya benar, saya akan mempertahankannya
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                              <input type="radio" name="SQ28" value="0" class="flat"> Saya suka menjadi anggota kelompok
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">29*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ29" value="1" class="flat"> Saya menghindari suatu perbedaan pendapat
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ29" value="0" class="flat"> Saya berusaha untuk lebih akrab dengan orang-orang
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">30*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                          <input type="radio" name="SQ30" value="1" class="flat"> Saya suka diberitahukan bagaimana melaksanakan tugas
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ30" value="0" class="flat"> Saya mudah merasa bosan
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                      <div class="form-group">
                                        <div class="col-md-9 col-sm-9  offset-md-3">
                                          <a class="btn btn-success pull-right" href="#tab_soal3" id="soal3-tab" role="tab" data-toggle="tab" aria-expanded="true">Selanjutnya</a>
                                          <!-- <a class="btn btn-warning pull-right" href="#tab_soal1" id="soal1-tab" role="tab" data-toggle="tab" aria-expanded="true">Kembali</a> -->
                                        </div>
                                      </div>

                                </div>
                              </div>
                            </div>
                          </div>
                            <!-- end recent activity -->
                          </div>

                          <div role="tabpanel" class="tab-pane fade " id="tab_soal3" aria-labelledby="soal3-tab">
                            <!-- start recent activity -->
                          <div class="row">
                            <div class="col-md-12 col-sm-12">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Bagian 3</h2>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <br />
                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">31*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ31" value="1" class="flat"> Saya bekerja keras
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ31" value="0" class="flat"> Saya banyak berfikir dan membuat rencana
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">32*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ32" value="1" class="flat"> Saya memimpin suatu kelompok
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ32" value="0" class="flat"> Hal-hal yang detail menarik minat saya
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">33*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ33" value="1" class="flat"> Saya membuat keputusan dengan mudah dan cepat
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ33" value="0" class="flat"> Saya merawat barang-barang saya dengan rapi dan teratur
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">34*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ34" value="1" class="flat"> Saya mengerjakan sesuatu dengan cepat
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ34" value="0" class="flat"> Saya jarang marah atau sedih
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">35*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ35" value="1" class="flat"> Saya ingin menjadi bagian dari suatu kelompok
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ35" value="0" class="flat"> Saya hanya ingin mengerjakan satu pekerjaan dalam satu saat
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">36*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ36" value="1" class="flat"> Saya berusaha membuat teman akrab
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ36" value="0" class="flat"> Saya berusaha keras untuk menjadi yang paling baik
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">37*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ37" value="1" class="flat"> Saya suka baju-baju dan mobil-mobil model mutakhir
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ37" value="0" class="flat"> Saya senang bertanggung jawab atas orang lain
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">38*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ38" value="1" class="flat"> Saya senang berdebat
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ38" value="0" class="flat"> Saya senang mendapat perhatian
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">39*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ39" value="1" class="flat"> Saya suka menyenangkan atasan-atasan saya
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ39" value="0" class="flat"> Saya tertarik untuk menjadi bagian dari suatu kelompok
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">40*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ40" value="1" class="flat"> Saya suka mematuhi peraturan dengan sungguh-sungguh
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ40" value="0" class="flat"> Saya ingin agar orang-orang benar-benar mengenal saya
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">41*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ41" value="1" class="flat"> Saya berusaha keras
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ41" value="0" class="flat"> Saya sangat ramah
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">42*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ42" value="1" class="flat"> Orang-orang menganggap saya adalah pemimpin yang baik
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ42" value="0" class="flat"> Saya berfikir panjang dan hati-hati
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">43*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ43" value="1" class="flat"> Saya sering mengambil kesempatan-kesempatan yang ada
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ43" value="0" class="flat"> Saya senang mengurusi hal-hal yang kecil
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">44*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ44" value="1" class="flat"> Orang-orang menganggap saya bekerja dengan cepat
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ44" value="0" class="flat"> Orang-orang menganggap saya merawat sesuatu dengan rapi
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">45*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ45" value="1" class="flat"> Saya suka berolahraga
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ45" value="0" class="flat"> Saya sangat menyenangkan
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                      <div class="form-group">
                                        <div class="col-md-9 col-sm-9  offset-md-3">
                                          <a class="btn btn-success pull-right" href="#tab_soal4" id="soal4-tab" role="tab" data-toggle="tab" aria-expanded="true">Selanjutnya</a>
                                          <!-- <a class="btn btn-warning pull-right" href="#tab_soal2" id="soal2-tab" role="tab" data-toggle="tab" aria-expanded="true">Kembali</a> -->
                                        </div>
                                      </div>

                                </div>
                              </div>
                            </div>
                          </div>
                            <!-- end recent activity -->
                          </div>

                          <div role="tabpanel" class="tab-pane fade " id="tab_soal4" aria-labelledby="soal4-tab">
                            <!-- start recent activity -->
                          <div class="row">
                            <div class="col-md-12 col-sm-12">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Bagian 4</h2>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <br />
                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">46*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ46" value="1" class="flat"> Saya senang jika orang-orang akrab dan ramah
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ46" value="0" class="flat"> Saya selalu berusaha menyelesaikan apa yang saya kerjakan
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">47*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ47" value="1" class="flat"> Saya suka bereksperimen dan mencoba hal-hal yang baru
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ47" value="0" class="flat"> Saya senang menyelesaikan dengan baik pekerjaan yang sulit
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">48*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ48" value="1" class="flat"> Saya senang diperlakukan secara adil
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ48" value="0" class="flat"> Saya suka memberitahu orang bagaimana mengerjakan sesuatu
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">49*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ49" value="1" class="flat"> Saya suka melakukan apa yang diharapkan dari saya
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ49" value="0" class="flat"> Saya senang mendapat perhatian
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">50*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ50" value="1" class="flat"> Saya suka penjelasan-penjelasan yang terperinci dalam bekerja
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ50" value="0" class="flat"> Saya senang berada diantara orang-orang
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">51*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ51" value="1" class="flat"> Saya selalu berusaha menyelesaikan tugas-tugas secara sempurna
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ51" value="0" class="flat"> Saya adalah orang yang tak mengenal lelah
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">52*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ52" value="1" class="flat"> Saya adalah tipe pemimpin
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ52" value="0" class="flat"> Saya mudah berteman
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">53*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ53" value="1" class="flat"> Saya suka mengambil kesempatan-kesempatan yang ada
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ53" value="0" class="flat"> Saya banyak berfikir
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">54*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ54" value="1" class="flat"> Saya bekerja dengan cepat dan mantap
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ54" value="0" class="flat"> Saya senang bekerja sampai pada hal yang sekecil-kecilnya
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">55*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ55" value="1" class="flat"> Saya mempunyai banyak tenaga untuk berolah raga dan bermain
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ55" value="0" class="flat"> Saya merawat barang-barang saya dengan rapi dan teratur
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">56*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ56" value="1" class="flat"> Saya berhubungan baik dengan semua orang
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ56" value="0" class="flat"> Saya bertabiat mantap dan tenang
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">57*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ57" value="1" class="flat"> Saya suka bertemu dengan orang-orang baru dan mengerjakan sesuatu yang baru
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ57" value="0" class="flat"> Saya selalu ingin menyelesaikan pekerjaan yang saya mulai
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">58*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ58" value="1" class="flat"> Saya biasanya mempertahankan apa yang saya yakini
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ58" value="0" class="flat"> Saya biasanya suka bekerja keras
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">59*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ59" value="1" class="flat"> Saya senang saran-saran dari orang yang saya hormati
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ59" value="0" class="flat"> Saya senang bertanggung jawab atas orang lain
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">60*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ60" value="1" class="flat"> Saya membiarkan orang lain mempengaruhi saya
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ60" value="0" class="flat"> Saya senang mendapatkan banyak perhatian
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                      <div class="form-group">
                                        <div class="col-md-9 col-sm-9  offset-md-3">
                                          <a class="btn btn-success pull-right" href="#tab_soal5" id="soal5-tab" role="tab" data-toggle="tab" aria-expanded="true">Selanjutnya</a>
                                          <!-- <a class="btn btn-warning pull-right" href="#tab_soal2" id="soal2-tab" role="tab" data-toggle="tab" aria-expanded="true">Kembali</a> -->
                                        </div>
                                      </div>

                                </div>
                              </div>
                            </div>
                          </div>
                            <!-- end recent activity -->
                          </div>

                          <div role="tabpanel" class="tab-pane fade " id="tab_soal5" aria-labelledby="soal5-tab">
                            <!-- start recent activity -->
                          <div class="row">
                            <div class="col-md-12 col-sm-12">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Bagian 5</h2>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <br />
                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">61*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ61" value="1" class="flat"> Saya biasanya bekerja keras
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ61" value="0" class="flat"> Saya biasanya bekerja cepat
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">62*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ62" value="1" class="flat"> Bila saya berbicara orang orang mendengarkan
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ62" value="0" class="flat"> Saya sangat mahir menggunakan peralatan
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">63*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ63" value="1" class="flat"> Saya lamban dalam membuat persahabatan
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ63" value="0" class="flat"> Saya lamban dalam memutuskan sesuatu
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">64*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ64" value="1" class="flat"> Saya biasanya makan dengan cepat
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ64" value="0" class="flat"> Saya senang membaca
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">65*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ65" value="1" class="flat"> Saya menyukai pekerjaan dimana saya dapat berkeliling
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ65" value="0" class="flat"> Saya senang pada pekerjaan yang membutuhkan ketelitian
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">66*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ66" value="1" class="flat"> Saya mencari teman sebanyak mungkin
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ66" value="0" class="flat"> Saya tahu hal-hal apa yang tidak perlu
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">67*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ67" value="1" class="flat"> Saya merencanakan sesuatu jauh-jauh sebelumnya
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ67" value="0" class="flat"> Saya selalu menyenangkan
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">68*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ68" value="1" class="flat"> Saya bangga akan ketenaran saya
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ68" value="0" class="flat"> Saya memusatkan perhatian pada satu persoalan sampai persoalan tersebut terpecahkan
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">69*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ69" value="1" class="flat"> Saya suka menyenangkan orang-orang yang saya hormati
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ69" value="0" class="flat"> Saya ingin berhasil
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">70*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ70" value="1" class="flat"> Saya senang orang lain yang membuat keputusan untuk kelompok
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ70" value="0" class="flat"> Saya suka membuat keputusan untuk kelompok
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">71*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ71" value="1" class="flat"> Saya selalu berusaha keras
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ71" value="0" class="flat"> Saya memutuskan sesuatu dengan mudah dan cepat
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">72*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ72" value="1" class="flat"> Kelompok biasanya menjalankan apa yang saya inginkan
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ72" value="0" class="flat"> Saya terlalu tergesa-gesa
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">73*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ73" value="1" class="flat"> Saya sering merasa lelah
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ73" value="0" class="flat"> Saya lamban dalam memutuskan sesuatu
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">74*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ74" value="1" class="flat"> Saya bekerja dengan cepat
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ74" value="0" class="flat"> Saya mudah berteman
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">75*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ75" value="1" class="flat"> Saya biasanya bersemangat dan bergairah
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ75" value="0" class="flat"> Saya memerlukan banyak waktu untuk berfikir
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="ln_solid"></div>
                                      <div class="form-group">
                                        <div class="col-md-9 col-sm-9  offset-md-3">
                                          <a class="btn btn-success pull-right" href="#tab_soal6" id="soal6-tab" role="tab" data-toggle="tab" aria-expanded="true">Selanjutnya</a>
                                          <!-- <a class="btn btn-warning pull-right" href="#tab_soal2" id="soal2-tab" role="tab" data-toggle="tab" aria-expanded="true">Kembali</a> -->
                                        </div>
                                      </div>

                                </div>
                              </div>
                            </div>
                          </div>
                            <!-- end recent activity -->
                          </div>

                          <div role="tabpanel" class="tab-pane fade " id="tab_soal6" aria-labelledby="soal6-tab">
                            <!-- start recent activity -->
                          <div class="row">
                            <div class="col-md-12 col-sm-12">
                              <div class="x_panel">
                                <div class="x_title">
                                  <h2>Bagian 6</h2>
                                  <div class="clearfix"></div>
                                </div>
                                <div class="x_content">
                                  <br />
                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">76*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ76" value="1" class="flat"> Saya sangat ramah kepada orang
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ76" value="0" class="flat"> Saya senang pada pekerjaan yang membutuhkan ketepatan
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">77*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ77" value="1" class="flat"> Saya banyak sekali berfikir dan membuat rencana
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ77" value="0" class="flat"> Saya meletakkan segalanya pada tempatnya
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">78*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ78" value="1" class="flat"> Saya senang pada pekerjaan yang membutuhkan ketelitian
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ78" value="0" class="flat"> Saya tidak mudah marah
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">79*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ79" value="1" class="flat"> Saya suka menurut pada orang-orang yang saya kagumi
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ79" value="0" class="flat"> Saya selalu menyelesaikan pekerjaan yang saya mulai
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">80*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ80" value="1" class="flat"> Saya senang mengikuti petunjuk-petunjuk yang jelas
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ80" value="0" class="flat"> Saya senang bekerja keras
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">81*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ81" value="1" class="flat"> Saya berusaha mendapatkan apa yang saya inginkan
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ81" value="0" class="flat"> Saya seorang pemimpin yang baik
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">82*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ82" value="1" class="flat"> Saya menyuruh orang untuk bekerja keras
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ82" value="0" class="flat"> Saya adalah orang yang kurang berfikir panjang
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">83*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ83" value="1" class="flat"> Saya memutuskan sesuatu dengan cepat
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ83" value="0" class="flat"> Saya berbicara dengan cepat
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">84*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ84" value="1" class="flat"> Saya biasa bekerja tergesa-gesa
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ84" value="0" class="flat"> Saya berlatih / berolah raga dengan teratur
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">85*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ85" value="1" class="flat"> Saya tidak suka menjumpai orang
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ85" value="0" class="flat"> Saya cepat merasa lelah
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">86*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ86" value="1" class="flat"> Saya banyak membuat persahabatan
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ86" value="0" class="flat"> Saya memerlukan banyak waktu untuk berfikir
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">87*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ87" value="1" class="flat"> Saya suka menerapkan teori-teori dalam bekerja
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ87" value="0" class="flat"> Saya suka mengerjakan sesuatu sampai pada hal yang sekecil-kecilnya
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">88*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ88" value="1" class="flat"> Saya suka mengerjakan sesuatu sampai pada hal yang sekecil-kecilnya
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ88" value="0" class="flat"> Saya suka mengatur pekerjaan saya
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">89*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ89" value="1" class="flat"> Saya meletakkan segalanya pada tempatnya
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ89" value="0" class="flat"> Saya selalu menyenangkan
                                          </label>
                                        </div>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <label class="col-md-1 col-sm-1  control-label">90*
                                      </label>
                                      <div class="col-md-9 col-sm-9 ">
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ90" value="1" class="flat"> Saya senang diberi petunjuk apa yang harus dikerjakan
                                          </label>
                                        </div>
                                        <div class="radio">
                                          <label>
                                            <input type="radio" name="SQ90" value="0" class="flat"> Saya harus menyelesaikan apa yang saya mulai
                                          </label>
                                        </div>
                                      </div>
                                    </div>
                                    </form>

                                    <div class="ln_solid"></div>
                                      <div class="form-group">
                                        <div class="col-md-9 col-sm-9  offset-md-3">
                                          <!-- <button type="button" class="btn btn-primary">Cancel</button>
                                          <button type="reset" class="btn btn-primary">Reset</button> -->
                                          <button type="submit" class="btn btn-success pull-right">Submit</button>
                                        </div>
                                      </div>

                                </div>
                              </div>
                            </div>
                          </div>
                            <!-- end recent activity -->
                          </div>

                        </div>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
  </div>
            
@elseif($cek <= 0 && $cekall < 10) 
  <div class="animated flipInY col-lg-12 col-md-12 col-sm-12  ">
    <div class="tile-stats">
    <a href="{{ route('personal.home') }}">
      <div class="icon"><i class="fa fa-plus-square-o"></i>
    </div>
    <div class="count">Hai, {{ Auth::user()->nama_lengkap}}</div>
      <h3>Anda Belum Melengkapi Form Lamaran</h3>
      <h3>Silahkan Cek Form Lamaran Pada Menu Home</h3>
      <h3>Form Lamaran Yang Belum Anda Isi Memiliki Tanda + </h3>
      <br>
      <h3>Test Kepribadian Bisa Anda Kerjakan</h3>
      <h3>Ketika Anda Telah Mengisi Semua Form Lamaran</h3>
    </div>
  </div>

@else
  <div class="animated flipInY col-lg-12 col-md-12 col-sm-12  ">
    <div class="tile-stats">
      <div class="icon"><i class="fa fa-check-square-o"></i>
    </div>
    <div class="count">Terimakasih, {{ Auth::user()->nama_lengkap}}</div>
      <h3>Anda Telah Mengerjakan Test Kepribadian</h3>
    </div>
  </div>

@endif

@endsection

<!-- End Multi step form -->
