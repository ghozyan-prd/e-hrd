<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="icon" href="../favicon.ico" type="image/ico" />

    <title>PT Davinti Indonesia</title>

    <!-- Bootstrap -->
    <link href="/material/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/material/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="/material/vendors/nprogress/nprogress.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="/material/vendors/iCheck/skins/flat/green.css" rel="stylesheet">
    <!-- bootstrap-wysiwyg -->
    <link href="/material/vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">
    <!-- Select2 -->
    <link href="/material/vendors/select2/dist/css/select2.min.css" rel="stylesheet">
    <!-- Switchery -->
    <link href="/material/vendors/switchery/dist/switchery.min.css" rel="stylesheet">
    <!-- starrr -->
    <link href="/material/vendors/starrr/dist/starrr.css" rel="stylesheet">
    <link href="/wizard/wizard.css" rel="stylesheet">

    <!-- Datatables -->

    <link href="/material/vendors/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="/material/vendors/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="/material/vendors/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="/material/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="/material/vendors/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">


    <!-- bootstrap-progressbar -->
    <link href="/material/vendors/bootstrap-progressbar/css/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet">
    <!-- JQVMap -->
    <link href="/material/vendors/jqvmap/dist/jqvmap.min.css" rel="stylesheet"/>
    <!-- bootstrap-daterangepicker -->
    <link href="/material/vendors/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">
    <!-- jQuery custom content scroller -->
    <link href="/material/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.min.css" rel="stylesheet"/>


    <!-- Custom Theme Style -->
    <link href="/material/build/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="nav-md">
  <div class="container body">
    <div class="main_container">
      <div class="col-md-3 left_col menu_fixed">
        <div class="left_col scroll-view">
          <div class="navbar nav_title" style="border: 0;">
            <a href="index.html" class="site_title"><i class="fa fa-cogs"></i> <span>Davinti Indonesia</span></a>
          </div>

          <div class="clearfix"></div>

          <!-- menu profile quick info -->
          <div class="profile clearfix">
            <div class="profile_pic">
              @if(empty(Auth::user()->foto))<img src="../images/no_pict.png" alt="..." class="img-circle profile_img">
                @else <img src="{{url('images/personal/' . Auth::user()->foto)}}" alt="..." class="img-circle profile_img" height="70" width="80">
              @endif
            </div>
            <div class="profile_info">
              <span>{{ date('l, d-m-Y') }}</span>
              <h2>{{ Auth::user()->nama_lengkap}}</h2>
            </div>
          </div>
          <!-- /menu profile quick info -->

          <br />

          <!-- sidebar menu -->
          <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
            <div class="menu_section">
              <h3>Dashboard Personal</h3>
              <ul class="nav side-menu">


                <li><a href="{{ route('personal.home') }}"><i class="fa fa-home"></i> Home</a>
                </li>

                <li><a><i class="fa fa-table"></i> Data Postulant <span class="fa fa-chevron-down"></span></a>
                  <ul class="nav child_menu">
                    <li><a href="{{ route('personal.data') }}">Data Diri</a></li>
                    <li><a href="{{ route('personal.family') }}">Data Keluarga</a></li>
                    <li><a href="{{ route('personal.education') }}">Data Pendidikan</a></li>
                    <li><a href="{{ route('personal.old_job') }}">Riwayat Pekerjaan</a></li>
                    <li><a href="{{ route('personal.training') }}">Pelatihan</a></li>
                    <li><a href="{{ route('personal.organization') }}">Pengalaman Organisasi</a></li>
                    <li><a href="{{ route('personal.award') }}">Prestasi</a></li>
                    <li><a href="{{ route('personal.language') }}">Penguasaan Bahasa</a></li>
                    <li><a href="{{ route('personal.reference') }}">Referensi</a></li>
                    <li><a href="{{ route('personal.emergencycontact') }}">Kontak Darurat</a></li>
                  </ul>
                </li>
                <li><a><i class="fa fa-clone"></i>Test Kepribadian <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                      <li><a href="{{ route('personal.test') }}">Mulai Tes</a></li>
                    </ul>
                  </li>
              </ul>
            </div>
          </div>
          <!-- /sidebar menu -->
        </div>
      </div>

      <!-- top navigation -->
      <div class="top_nav">
          <div class="nav_menu">
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
              <nav class="nav navbar-nav">
              <ul class=" navbar-right">
                <li class="nav-item dropdown open" style="padding-left: 15px;">
                  <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
                    @if(empty(Auth::user()->foto))<img src="../images/no_pict.png" alt="">{{ Auth::user()->nama_lengkap }}
                      @else <img src="{{url('images/personal/' . Auth::user()->foto)}}" alt="">{{ Auth::user()->nama_lengkap }}
                    @endif
                  </a>
                  <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item"  href="javascript:;"> Profile</a>
                      <a class="dropdown-item"  href="javascript:;">
                        <span class="badge bg-red pull-right">50%</span>
                        <span>Settings</span>
                      </a>
                  <a class="dropdown-item"  href="javascript:;">Help</a>
                    <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                              document.getElementById('logout-form').submit();"><i class="fa fa-sign-out pull-right"></i>
                                        {{ __('Log out') }}
                    </a>

                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                    </form>
                  </div>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      <!-- /top navigation -->

      <!-- page content -->
      <div class="right_col" role="main">
        <div class="">
            @yield('content')

        </div>
      </div>
        <!-- /page content -->
        <!-- page content -->

        <!-- /page content -->

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            Gentelella - Bootstrap Admin Template by <a href="https://colorlib.com">Colorlib</a>
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="/material/vendors/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="/material/vendors/bootstrap/dist/js/bootstrap.bundle.min.js"></script>
    <!-- FastClick -->
    <script src="/material/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="/material/vendors/nprogress/nprogress.js"></script>
    <!-- Chart.js -->
    <script src="/material/vendors/Chart.js/dist/Chart.min.js"></script>
    <!-- gauge.js -->
    <script src="/material/vendors/gauge.js/dist/gauge.min.js"></script>
    <!-- bootstrap-progressbar -->
    <script src="/material/vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="/material/vendors/iCheck/icheck.min.js"></script>
    <!-- Skycons -->
    <script src="/material/vendors/skycons/skycons.js"></script>
    <!-- Datatables -->
    <script src="/material/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="/material/vendors/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script src="/material/vendors/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="/material/vendors/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="/material/vendors/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="/material/vendors/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="/material/vendors/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="/material/vendors/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="/material/vendors/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="/material/vendors/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="/material/vendors/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="/material/vendors/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="/material/vendors/jszip/dist/jszip.min.js"></script>
    <script src="/material/vendors/pdfmake/build/pdfmake.min.js"></script>
    <script src="/material/vendors/pdfmake/build/vfs_fonts.js"></script>

    <!-- Flot -->
    <script src="/material/vendors/Flot/jquery.flot.js"></script>
    <script src="/material/vendors/Flot/jquery.flot.pie.js"></script>
    <script src="/material/vendors/Flot/jquery.flot.time.js"></script>
    <script src="/material/vendors/Flot/jquery.flot.stack.js"></script>
    <script src="/material/vendors/Flot/jquery.flot.resize.js"></script>
    <!-- Flot plugins -->
    <script src="/material/vendors/flot.orderbars/js/jquery.flot.orderBars.js"></script>
    <script src="/material/vendors/flot-spline/js/jquery.flot.spline.min.js"></script>
    <script src="/material/vendors/flot.curvedlines/curvedLines.js"></script>
    <!-- DateJS -->
    <script src="/material/vendors/DateJS/build/date.js"></script>
    <!-- JQVMap -->
    <script src="/material/vendors/jqvmap/dist/jquery.vmap.js"></script>
    <script src="/material/vendors/jqvmap/dist/maps/jquery.vmap.world.js"></script>
    <script src="/material/vendors/jqvmap/examples/js/jquery.vmap.sampledata.js"></script>
    <!-- bootstrap-daterangepicker -->
    <script src="/material/vendors/moment/min/moment.min.js"></script>
    <script src="/material/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- jQuery custom content scroller -->
    <script src="/material/vendors/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- jQuery Smart Wizard -->
    <script src="/material/vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
    <!-- bootstrap-wysiwyg -->
    <script src="/material/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
    <script src="/material/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
    <script src="/material/vendors/google-code-prettify/src/prettify.js"></script>
    <!-- jQuery Tags Input -->
    <script src="/material/vendors/jquery.tagsinput/src/jquery.tagsinput.js"></script>
    <!-- Switchery -->
    <script src="/material/vendors/switchery/dist/switchery.min.js"></script>
    <!-- Select2 -->
    <script src="/material/vendors/select2/dist/js/select2.full.min.js"></script>
    <!-- Parsley -->
    <script src="/material/vendors/parsleyjs/dist/parsley.min.js"></script>
    <!-- Autosize -->
    <script src="/material/vendors/autosize/dist/autosize.min.js"></script>
    <!-- jQuery autocomplete -->
    <script src="/material/vendors/devbridge-autocomplete/dist/jquery.autocomplete.min.js"></script>
    <!-- starrr -->
    <script src="/material/vendors/starrr/dist/starrr.js"></script>
    <!-- Custom Theme Scripts -->
    <script src="/material/build/js/custom.min.js"></script>
    <script src="/wizard/wizard.js"></script>
    @yield('footer')
    @yield('chart')
    @yield('org')


    @include('sweetalert::alert')
  </body>
</html>
