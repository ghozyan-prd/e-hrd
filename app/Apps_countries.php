<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Apps_countries extends Model
{
  protected $table = "apps_countries";
  protected $fillable = ['country_code','country_name'];
}
