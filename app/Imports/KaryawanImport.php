<?php

namespace App\Imports;

use App\Salary;
use Maatwebsite\Excel\Concerns\ToModel;

use Maatwebsite\Excel\Concerns\WithStartRow;

class KaryawanImport implements ToModel,WithStartRow

{
    
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

   
    public function model( array $row)
    {
       
      if ($row[3] != "0" ){
        return new Salary([
        
            'ID_Pegawai' => $row[2],
            'Nama_Pegawai' => $row[3],
            'Kehadiran' => $row[10], 
            'Jumlah_Cuti_Tak_Dibayar' => $row[11], 
            'Gaji_Pensiun' => $row[13],
            'Tunjangan_Harian' => $row[14], 
            'Tunjangan_Lainnya_Uang_Lembur_dsb' => $row[15], 
            'Cuti_Tidak_Dibayar' => $row[17],
            'Bonus_Tak_Tetap' => $row[19], 
            'Jumlah_Penghasilan_Bruto' => $row[22], 
            'Jumlah_Pengurangan' => $row[23], 
            'PPh_21_sebulan' => $row[31],
            'Uang_Pembayaran_Gaji' => $row[33], 
           
        ]);

      }
       
    }
    public function startRow(): int
    {
        return 15;
    }
    
} 

