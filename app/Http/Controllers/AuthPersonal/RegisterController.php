<?php

namespace App\Http\Controllers\AuthPersonal;

use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use App\Postulant;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use Intervention\Image\ImageManagerStatic;

class RegisterController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('postulant.personal.data');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
     public function create()
     {
         return view('authPersonal.register');
     }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
     public function store(Request $request)
     {
       Postulant::create([
       'nama_lengkap' => $request->name,
       'email' => $request->email,
       'password' => Hash::make($request->password),
       'kebangsaan' => 'Indonesia',
     ]);
     //dd($request);
     Alert::success('Registrasi Berhasil', 'Success');
     return redirect('/personal/login')-> with('Alert');
     }

    /**
     * Display the specified resource.
     *
     * @param  \App\Postulant  $postulant
     * @return \Illuminate\Http\Response
     */
    public function show(Postulant $postulant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Postulant  $postulant
     * @return \Illuminate\Http\Response
     */
    public function edit(Postulant $postulant)
    {
      //dd($postulant);
      if (Auth::user()){
        $personal = Postulant::find(Auth::user()->id);
        $dropdown = Auth::User()->posisi_yang_dilamar;
        $countries = Auth::User()->kebangsaan;
        $gender = Auth::User()->jenis_kelamin;
        $status = Auth::User()->status_perkawinan;
        $laptop = Auth::User()->laptop;
        $agama = Auth::User()->agama;
        $kendaraan = Auth::User()->kendaraan;
        //dd($countries);
        return view('postulant.personal.data',compact('personal','dropdown','gender','status','laptop','agama','kendaraan','countries'));
      }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Postulant  $postulant
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Postulant $postulant)
    {


       $Postulant = Postulant::findOrFail(auth::user()->id);
       $Postulant-> nama_lengkap = $request->input('nama_lengkap');
       $Postulant-> posisi_yang_dilamar = $request->input ('posisi_yang_dilamar');
       $Postulant-> tempat_lahir = $request->input ('tempat_lahir');
       $Postulant-> tanggal_lahir = $request->input ('tanggal_lahir');
       $Postulant-> no_ktp = $request->input ('no_ktp');
       $Postulant-> jenis_kelamin = $request->input ('jenis_kelamin');

       $Postulant-> status_perkawinan = $request->input ('status_perkawinan');
       $Postulant-> agama = $request->input ('agama');
       $Postulant-> kebangsaan = $request->input ('kebangsaan');
       $Postulant-> tanggal_join = $request->input ('tanggal_join');
       $Postulant-> npwp = $request->input ('npwp');

       $Postulant-> alamat_domisili = $request->input ('alamat_domisili');
       $Postulant-> kota_domisili = $request->input ('kota_domisili');
       $Postulant-> alamat_ktp = $request->input ('alamat_ktp');
       $Postulant-> kota_ktp = $request->input ('kota_ktp');
       $Postulant-> telepon = $request->input ('telepon');
       $Postulant-> email = $request->input ('email');
       $Postulant-> kendaraan = $request->input ('kendaraan');

       $Postulant-> laptop = $request->input ('laptop');
       $Postulant-> gaji_diharap = $request->input ('gaji_diharap');
       $Postulant-> anak_ke = $request->input ('anak_ke');
       $Postulant-> saudara = $request->input ('saudara');
       $Postulant-> pencapaian_sudah_dilakukan = $request->input ('pencapaian_sudah_dilakukan');
       $Postulant-> pencapaian_yang_diinginkan = $request->input ('pencapaian_yang_diinginkan');
       $Postulant-> status_lamaran = $request->input ('status_lamaran');
       $Postulant-> alasan = $request->input ('alasan');

       if ($request->has('image')) {
           // Get image file
           $image = $request->file('image');

           $imageName = time().'.'.$request->image->extension();
           // $image_resize = Image::make($image->getRealPath());
           // $image_resize->resize(300,300);
           // $image_resize->save(public_path('images/personal').$imageName);
           //$Postulant = Image::make($image->getRealPath())->resize(200, 200)->save($path);
           $request->image->move(public_path('images/personal'), $imageName);

           $Postulant->foto = $imageName;
       }
       $Postulant->save();
     //dd($request);
      Alert::success('Berhasil di Update', 'Success');
        return redirect('/personal/data')-> with('Alert');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Postulant  $postulant
     * @return \Illuminate\Http\Response
     */
    public function destroy(Postulant $postulant)
    {
        //
    }
}
