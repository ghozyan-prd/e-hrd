<?php

namespace App\Http\Controllers\AuthPersonal;
use RealRashid\SweetAlert\Facades\Alert;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    public function __construct()
    {
        $this->middleware('guest:personal')->except('logout');
    }

    public function showLoginForm()
    {
        return view('authPersonal.login');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required|string'
        ]);

        $credential = [
            'email' => $request->email,
            'password' => $request->password
        ];

        // Attempt to log the user in
        if (Auth::guard('personal')->attempt($credential, $request->member)){
            // If login succesful, then redirect to their intended location
            Alert::success('Login Berhasil','success' );
            return redirect()->intended(route('personal.home'))->with('Alert');
        }

        // If Unsuccessful, then redirect back to the login with the form data
        Alert::error('Login Gagal', 'Periksa Username / Password Anda');
        return redirect()->back()->with('Alert');
    }

    public function logout()
    {
        Auth::guard('personal')->logout();
        return $this->loggedOut($request) ?: redirect('/');
    }
}
