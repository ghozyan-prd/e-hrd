<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Salary;
use App\Employee;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SalaryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $salary = DB::table('salaries')
      ->select('tahun','bulan', DB::raw('SUM(Uang_Pembayaran_Gaji) as gaji'))
      ->groupBy('tahun','bulan')
      ->get();
      //dd($salary);
        return view('admin.salary.index', compact('salary'));
    }


    public function tabel(Request $request)
    {
      $bulan = DB::table('salaries')
      ->select('salaries.Bulan')
      ->groupBy('bulan')
      ->get();

      $tahun = DB::table('salaries')
      ->select('salaries.Tahun')
      ->groupBy('tahun')
      ->get();



      $detil = DB::table('salaries')
      ->select('salaries.Tahun','salaries.Bulan','salaries.Nama_Pegawai','salaries.Gaji_Pensiun','salaries.Jumlah_Penghasilan_Bruto', 'salaries.Jumlah_Pengurangan', 'salaries.Uang_Pembayaran_Gaji', 'employees.nama')
      ->join('employees','salaries.Nama_Pegawai','employees.nama')
      ->where('salaries.Tahun', $request->tahun)
      ->where('salaries.Bulan', $request->bulan)
      ->get();

      $salary = DB::table('salaries')
      ->select('salaries.Tahun','salaries.Bulan','salaries.Nama_Pegawai','salaries.Gaji_Pensiun','salaries.Jumlah_Penghasilan_Bruto', 'salaries.Jumlah_Pengurangan', 'salaries.Uang_Pembayaran_Gaji', 'employees.nama')
      ->join('employees','salaries.Nama_Pegawai','employees.nama')
      ->get();

      //dd($salary);
        return view('admin.salary.show',compact('salary'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $gaji= Salary::all();
      //$bulan = array(
        //         '01' => 'Januari',
        //         '02' => 'Februari',
        //         '03' => 'Maret',
        //         '04' => 'April',
        //         '05' => 'Mei',
        //         '06' => 'Juni',
        //         '07' => 'Juli',
        //         '08' => 'Agustus',
        //         '09' => 'September',
        //         '10' => 'Oktober',
        //         '11' => 'November',
        //         '12' => 'Desember',
        // );
        //
        // $gajiterhitung = DB::table('salaries')
        //   ->select('salaries.id', 'salaries.tahun','salaries.bulan','salaries.id_pegawai','salaries.pengurangan_kehadiran','salaries.maks_kehadiran',
        //   'salaries.gaji_pokok', 'salaries.tunjangan_harian','salaries.tunjangan_transport', 'salaries.hak_tunjangan',
        //   'salaries.total_tunjangan','salaries.bonus_tak_tetap', 'salaries.total_gaji_bruto','salaries.pph21', 'salaries.total_cuti_tak_dibayar',
        //   'salaries.potongan_cuti_tak_dibayar','salaries.bpjs_kes', 'salaries.bpjs_ket','salaries.total_potongan', 'salaries.total_gaji_diterima', 'employees.nama')
        //   ->join('employees','salaries.id_pegawai','employees.id')
        //   ->whereYear('salaries.created_at', date('Y'))
        //   ->whereMonth('salaries.created_at', date('m'))
        //   ->orderBy('salaries.created_at', 'DESC')
        //   ->get();
        //

        return view('admin.salary.create', compact('gaji'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $jatah = DB::table('employees')
      ->select('employees.jatah_cuti')
      ->where('employees.id_postulant', $request->id_pegawai)
      ->sum('jatah_cuti');
      //$tes= array($jatah);

      $terpakai = DB::table('cuti_requests')
      ->select('cuti_requests.cuti_terpakai')
      ->where('cuti_requests.id_karyawan', $request->id_pegawai)
      ->where('keputusan_hrd','=' ,'diterima')
      ->where('keputusan_atasan','=' ,'diterima')
      ->whereYear('tanggal_cuti', date('Y'))
      ->sum('cuti_terpakai');

      $terpakai_sekarang = DB::table('cuti_requests')
      ->select('cuti_requests.cuti_terpakai')
      ->where('cuti_requests.id_karyawan', $request->id_pegawai)
      ->where('keputusan_hrd','=' ,'diterima')
      ->where('keputusan_atasan','=' ,'diterima')
      ->whereYear('tanggal_cuti', date('Y'))
      ->whereMonth('tanggal_cuti', date('m'))
      ->sum('cuti_terpakai');
      // dd($request->id_pegawai);

      $gaji_pokok = DB::table('employees')
      ->select('employees.gaji_pokok')
      ->where('employees.id_postulant', $request->id_pegawai)
      ->sum('gaji_pokok');

      $bpjskes = DB::table('employees')
      ->select('employees.bpjs_kes')
      ->where('employees.id_postulant', $request->id_pegawai)
      ->sum('bpjs_kes');

      $bpjsket = DB::table('employees')
      ->select('employees.bpjs_ket')
      ->where('employees.id_postulant', $request->id_pegawai)
      ->sum('bpjs_ket');

      // dd($bpjskes, $bpjsket);

      $tunjangan_harian = DB::table('employees')
      ->select('employees.tunjangan_harian')
      ->where('employees.id_postulant', $request->id_pegawai)
      ->sum('tunjangan_harian');

      $tunjangan_transport = DB::table('employees')
      ->select('employees.tunjangan_transport')
      ->where('employees.id_postulant', $request->id_pegawai)
      ->sum('tunjangan_transport');

      $gaji_pokok = DB::table('employees')
      ->select('employees.gaji_pokok')
      ->where('employees.id_postulant', $request->id_pegawai)
      ->sum('gaji_pokok');

      $ozy= "Berhasil";
      $nol = 0;
      $cek_jatah_cuti = $jatah-$terpakai;
      $cuti_terpakai_lalu = $terpakai - $terpakai_sekarang;
      $cek_jatah_cuti_lalu = $jatah-$cuti_terpakai_lalu;
      $maksimal_kehadiran = $request->maks_kehadiran;
      $kurang_hadir = $request->pengurangan_kehadiran;

      // dd($terpakai, $terpakai_sekarang, $cek_jatah_cuti, $cuti_terpakai_lalu, $cek_jatah_cuti_lalu, $maksimal_kehadiran, $kurang_hadir, $terpakai_sekarang);

      if($kurang_hadir>0 && $cek_jatah_cuti_lalu>0 && $terpakai_sekarang >= 0){
        if($cek_jatah_cuti_lalu - $kurang_hadir - $terpakai_sekarang >=0){
          $ctd = 0;
        }
        else{
          $ctd = (abs($cek_jatah_cuti_lalu - $kurang_hadir  - $terpakai_sekarang ));
        }
      }

      elseif($kurang_hadir>0 && $cek_jatah_cuti_lalu<=0){
        $ctd = $kurang_hadir + $terpakai_sekarang;
      }

      else{
        $ctd = 0;
      }
      $total_cuti_tak_dibayar = $ctd;
      // dd($ctd);

      $potongan_gaji_tak_dibayar = $ctd * ($gaji_pokok / 21.75);

      $hak_tunjangan_kehadiran = $request->hak_tunjangan;
      $bonustaktetap = $request->bonus_tak_tetap;
      $pph_21 = $request->pph21;
      $total_gaji_bruto = ($hak_tunjangan_kehadiran * ($tunjangan_harian+$tunjangan_transport))+$gaji_pokok+$bonustaktetap;
      $total_tunjangan = ($hak_tunjangan_kehadiran * ($tunjangan_harian+$tunjangan_transport));
      $total_potongan = ($pph_21+$potongan_gaji_tak_dibayar+$bpjskes+$bpjsket);
      $gaji_diterima = ($total_gaji_bruto-($pph_21+$potongan_gaji_tak_dibayar+$bpjskes+$bpjsket));

      // //dd($gaji_diterima);
      Salary::create([
        'tahun' => $request->tahun,
        'bulan' => $request->bulan,
        'id_pegawai' => $request->id_pegawai,
        'pengurangan_kehadiran' => $kurang_hadir,
        'maks_kehadiran' => $maksimal_kehadiran,
        'gaji_pokok' => $gaji_pokok,
        'tunjangan_harian' => $tunjangan_harian,
        'tunjangan_transport' => $tunjangan_transport,
        'hak_tunjangan' => $hak_tunjangan_kehadiran, //hak tunjangan belum, total tunjangan belum
        'total_tunjangan' => $total_tunjangan,
        'bonus_tak_tetap' => $request->bonus_tak_tetap,
        'total_gaji_bruto' => $total_gaji_bruto,
        'pph21' => $request->pph21,
        'total_cuti_tak_dibayar' => $ctd,
        'potongan_cuti_tak_dibayar' => $potongan_gaji_tak_dibayar,
        'bpjs_kes' => $bpjskes,
        'bpjs_ket' => $bpjsket,
        'total_potongan' => $total_potongan,
        'total_gaji_diterima' => $gaji_diterima,
      ]);
      // dd($gajiterhitung);
      return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function show(Salary $salary)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function edit(Salary $salary)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Salary $salary)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Salary  $salary
     * @return \Illuminate\Http\Response
     */
    public function destroy(Salary $salary)
    {
      $salary = Salary::destroy($salary->id);
      // dd($salary);
      return redirect('/admin/salary/create');
    }
}
