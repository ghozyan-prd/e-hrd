<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Corporate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CorporateGroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $corporate_groups = Corporate::all();
        return view('admin.corporate.index', compact('corporate_groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'namaCor' => 'required'
        ]);

        $id = DB::table('corporate_groups')->orderBy('id','DESC')->take(1)->get();
        foreach ($id as $value);
        $idlama = $value->id;
        $idbaru = $idlama + 1;
        $kode_corporate = 'Div-'.$idbaru;

        Corporate::create([
            'kode_corporate_group' => $kode_corporate,
            'nama_corporate_group' => $request->namaCor,
        ]);
        //dd($request);
        return redirect('/admin/corporate')-> with('status', 'Data Corporate Group Baru Berhasil di Tambahkan !!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Corporate  $corporate
     * @return \Illuminate\Http\Response
     */
    public function show(Corporate $corporate)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Corporate  $corporate
     * @return \Illuminate\Http\Response
     */
    public function edit(Corporate $corporate)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Corporate  $corporate
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //dd($request->all());

        $request->validate([
            'namaCor' => 'required'
        ]);

        Corporate::where('id',$request->idcorporate)
        ->update([
            'nama_corporate_group' => $request->namaCor
        ]);

        return redirect('/admin/corporate')-> with('edit', 'Data Corporate Group Baru Berhasil di Tambahkan !!');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Corporate  $corporate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Corporate $corporate)
    {
        Corporate::destroy($corporate->id);

        return redirect('/admin/corporate')-> with('delete', 'Data Corporate Group Berhasil di Hapus !!');
    }

    public function trash(){
        $corporate = Corporate::onlyTrashed()->get();
        return view ('admin.corporate.trash', ['corporates' => $corporate]);
    }

    public function restore($id){
        $corporate = Corporate::onlyTrashed()->where('id', $id);
        $corporate->restore();
        return redirect('/admin/corporate/trash')-> with('restore', 'Data Corporate Group Berhasil di Restore');
    }

    public function deleted_permanent($id){
        $corporate = Corporate::onlyTrashed()->where('id', $id);
        $corporate->forceDelete();
        return redirect('/admin/corporate/trash')-> with('delete', 'Data Corporate Group Berhasil di Delete Permanent');
    }

    public function restore_all(){
        $corporate = Corporate::onlyTrashed();
        $corporate->restore();
        return redirect('/admin/corporate/trash')-> with('restore_all', 'Data Corporate Group Berhasil di Restore all');
    }

    public function deleted_all(){
        $corporate = Corporate::onlyTrashed();
        $corporate->forceDelete();
        return redirect('/admin/corporate/trash')-> with('deleted_all', 'Data Corporate Group Berhasil di Delete Semua');
    }
}
