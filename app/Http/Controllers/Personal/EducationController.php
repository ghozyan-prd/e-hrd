<?php

namespace App\Http\Controllers\Personal;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use App\Education;
use Illuminate\Http\Request;
use Auth;

class EducationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $tampil = Education::where('id_postulant',Auth::User()->id)->get();
      //dd($tampil);
      return view('postulant.personal.education', compact('tampil'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      //dd($request);
      Education::create([
      'id_postulant' => Auth::User()->id,
      'jenis_pendidikan' => $request->jenis_pendidikan,
      'nama_sekolah' => $request->nama_sekolah,
      'jurusan' => $request->jurusan,
      'tahun_mulai' => $request->tahun_mulai,
      'tahun_selesai' => $request->tahun_selesai,
      ]);
      Alert::success('Berhasil di Tambahkan', 'Success');
      return redirect('/personal/education')-> with('Alert');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function show(Education $education)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function edit(Education $education)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Education $education)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Education  $education
     * @return \Illuminate\Http\Response
     */
    public function destroy(Education $education)
    {
      Education::destroy($education->id);
      Alert::warning('Data Berhasil Dihapus', 'Warning');
      return redirect('/personal/education')-> with('Alert');
    }
}
