<?php

namespace App\Http\Controllers\Personal;
use App\Http\Controllers\Controller;
use App\Postulant;
use App\Test;
use App\Employee;
use App\Family;
use App\Education;
use App\Job_history;
use App\Training;
use App\Organization;
use App\Award;
use App\Language;
use App\Reference;
use App\Emergencycontact;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PersonalController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth:personal');
    }


    public function index()
    {
        $cekpost = Postulant::where('posisi_yang_dilamar', '!=', null)
        ->where('id', Auth::user()->id)
        ->count();

        if($cekpost>=1){
            $cekpost=1;
        }
        else{
            $cekpost=0;
        }

        $cekfam = Family::where('id_postulant', Auth::user()->id)
        ->count();

        if($cekfam>=1){
            $cekfam=1;
        }
        else{
            $cekfam=0;
        }

        $cekedu = Education::where('id_postulant', Auth::user()->id)
        ->count();

        if($cekedu>=1){
            $cekedu=1;
        }
        else{
            $cekedu=0;
        }

        $cekjhis = Job_history::where('id_postulant', Auth::user()->id)
        ->count();

        if($cekjhis>=1){
            $cekjhis=1;
        }
        else{
            $cekjhis=0;
        }

        $cektra = Training::where('id_postulant', Auth::user()->id)
        ->count();

        if($cektra>=1){
            $cektra=1;
        }
        else{
            $cektra=0;
        }

        $cekorg = Organization::where('id_postulant', Auth::user()->id)
        ->count();

        if($cekorg>=1){
            $cekorg=1;
        }
        else{
            $cekorg=0;
        }

        $cekawd = Award::where('id_postulant', Auth::user()->id)
        ->count();

        if($cekawd>=1){
            $cekawd=1;
        }
        else{
            $cekawd=0;
        }

        $ceklang = Language::where('id_postulant', Auth::user()->id)
        ->count();

        if($ceklang>=1){
            $ceklang=1;
        }
        else{
            $ceklang=0;
        }

        $cekref = Reference::where('id_postulant', Auth::user()->id)
        ->count();

        if($cekref>=1){
            $cekref=1;
        }
        else{
            $cekref=0;
        }

        $cekemc = Emergencycontact::where('id_postulant', Auth::user()->id)
        ->count();

        if($cekemc>=1){
            $cekemc=1;
        }
        else{
            $cekemc=0;
        }

        $cektest = Test::where('id_postulant', Auth::user()->id)
        ->count();

        if($cektest>=1){
            $cektest=1;
        }
        else{
            $cektest=0;
        }

        $cekall = $cekpost + $cekfam + $cekedu + $cekjhis + $cektra + $cekorg + $cekawd + $ceklang + $cekref + $cekemc;

        // dd($cekall);

        return view('postulant.personal.index', compact('cekpost', 'cekfam', 'cekedu', 'cekjhis', 'cektra',
    'cekorg', 'cekawd', 'ceklang', 'cekref', 'cekemc', 'cekall', 'cektest'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Data $data)
    {

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
