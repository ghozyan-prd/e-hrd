<?php

namespace App\Http\Controllers\Personal;
use App\Http\Controllers\Controller;
use RealRashid\SweetAlert\Facades\Alert;
use App\Organization;
use Illuminate\Http\Request;
use Auth;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $tampil = Organization::where('id_postulant',Auth::User()->id)->get();
      //dd($tampil);
      return view('postulant.personal.organization', compact('tampil'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->cekorg == "no"){
        Organization::create([
          'id_postulant' => Auth::User()->id,
          'nama_organisasi' => "Tidak Memiliki Riwayat Organisasi"
          ]);
      }
      
      else{
        $request->validate([
          'nama_organisasi' => 'required',
          'jabatan' => 'required',
          'tahun_mulai' => 'required',
          'tahun_selesai' => 'required',
        ]);

        Organization::create([
          'id_postulant' => Auth::User()->id,
          'nama_organisasi' => $request->nama_organisasi,
          'jabatan' => $request->jabatan,
          'tahun_mulai' => $request->tahun_mulai,
          'tahun_selesai' => $request->tahun_selesai,
          ]);
      }

      Alert::success('Berhasil di Tambahkan', 'Success');
      return redirect('/personal/organization')-> with('Alert');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function show(Organization $organization)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function edit(Organization $organization)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Organization $organization)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Organization  $organization
     * @return \Illuminate\Http\Response
     */
    public function destroy(Organization $organization)
    {
      Organization::destroy($organization->id);
      Alert::warning('Data Berhasil Dihapus', 'Warning');
      return redirect('/personal/organization')-> with('Alert');
    }
}
