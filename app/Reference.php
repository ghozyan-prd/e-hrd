<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reference extends Model
{
  protected $table = "references";
  protected $fillable = ['id_postulant','nama', 'perusahaan', 'jabatan','telepon'];
}
