<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Education extends Model
{
  protected $table = "educations";
  protected $fillable = ['id_postulant','jenis_pendidikan', 'nama_sekolah', 'jurusan', 'tahun_mulai', 'tahun_selesai'];
}
