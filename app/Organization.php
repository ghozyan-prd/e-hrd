<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Organization extends Model
{
  protected $table = "organizations";
  protected $fillable = ['id_postulant','nama_organisasi', 'jabatan', 'tahun_mulai','tahun_selesai'];
}
