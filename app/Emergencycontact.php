<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Emergencycontact extends Model
{
  protected $table = "emergencycontacts";
  protected $fillable = ['id_postulant','nama', 'hubungan', 'alamat', 'telepon'];
}
