<?php

namespace App\Providers;
use App\Apps_countries;
use Illuminate\Support\ServiceProvider;

class DynamicCountries extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('*',function($view){
        $view->with('countries_array', Apps_countries::all());
      });
    }
}
